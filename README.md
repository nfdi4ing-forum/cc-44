# Community-Cluster 44 - Construction Engineering and Architecture

![](/img/cc-44-logo.png)

Discussion in the Context of Community Cluster 44 i.e. Research Area "Construction engineering and architecture"  
[Browse Threads or create a new one](https://git.rwth-aachen.de/nfdi4ing-forum/cc-44/-/issues)
